#!/bin/env python3
import argparse
import appoptics_metrics
import itertools
import logging
import json
import pyjq
import requests
import os
import subprocess
import xml.etree.ElementTree

# A name of a file, where API key is stored
logger = logging.getLogger(__name__)
_options = {}

class Datasource():
    """Source of measurement data."""
    def __init__(self):
        self.data_tags = {'datasource': 'unknown'}

    def read(self):
        """Read the data; yield (key, value) tuples."""
        raise NotImplementedError

    def __str__(self):
        return("%s tags=%s" % (self.__class__.__name__, self.data_tags))


class HostDatasource(Datasource):
    """Data from a Linux host, where the application is installed."""
    def __init__(self):
        super().__init__()
        self.data_tags = {'hw': 't460s'}

    def read(self):
        """Read all available data."""
        return itertools.chain(
            self._read_temperature_sensor_data(),
            self._read_loadaverage()
        )

    def _read_temperature_sensor_data(self):
        """Read data from sensors; return tuples of (sensor_name, value)."""
        sensors = subprocess.Popen('sensors -j'.split(), stdout=subprocess.PIPE)
        stdout = sensors.communicate()[0]
        sensor_data = json.loads(stdout.decode())

        input_query = (
            '.. | select(type == "object") '
            '| to_entries[] | select(."key" | endswith("input"))')

        input_readings = pyjq.apply(input_query, sensor_data)
        for reading in input_readings:
            yield reading['key'], reading['value']

    def _read_loadaverage(self):
        """Return load average data for 1 minute."""
        loadavg_1m, _, _ = os.getloadavg()
        yield 'loadavg_1m', loadavg_1m


class ActualWeatherDatasource(Datasource):
    """Read actual weather data from public API."""
    # Data fields to be retrieved
    _data_keys=('temp_c',)

    # Source of weather information
    _metar_api_url = (
        'https://www.aviationweather.gov/adds/dataserver_current/httpparam')

    def __init__(self, airport_code):
        super().__init__()
        self._airport_code = airport_code
        self.data_tags = {'airport_code': self._airport_code}

    def read(self):
        """Retrieve actual weather data."""
        response = requests.get(
            '%s?datasource=metars&requestType=retrieve&format=xml'
            '&mostRecentForEachStation=constraint&hoursBeforeNow=1.25'
            '&stationString=%s' % (self._metar_api_url, self._airport_code)) 
        rs = response.content.decode()
        et = xml.etree.ElementTree.fromstring(rs)
        for key in self._data_keys:
            reading = et.find('.//%s' % key).text
            yield key, float(reading)


def read_and_send(datasources):
    """Collect and send data from specified data sources."""
    api = _connect() if not _options.dry_run else None
    
    for ds in datasources:
        _send_data(api, ds)


def _connect():
    """Connect to AppOptics."""
    try:
        with open(_options.token_file) as keyfile:
            key = keyfile.readline().strip()
        api = appoptics_metrics.connect(key)
    except OSError as e:
        msg = "Unable to read API token file: " + str(e)
        logger.error(msg, exc_info=_options.verbose)
        exit(1)

    logger.info("Connected to %s", api.hostname)
    return api


def _send_data(api, datasource):
    """Read (key, value) pairs from data source, send data to AppOptics."""
    logger.info("Sending data from %s", datasource)
    for k, v in datasource.read():
        logger.debug('Sending a reading "%s: %s"', k, v)
        if not _options.dry_run:
            try:
                api.submit(k, v, tags=datasource.data_tags)
            except Exception as e:
                logger.error(e, exc_info=_options.verbose)
                exit(1)


def _read_args():
    """Read command line arguments."""
    parser = argparse.ArgumentParser(description='AppOptics demo application.')
    parser.add_argument(
        '--location', dest='location', default='LKTB',
        help='Set ICAO airport code of the host"s physical location.')
    parser.add_argument(
        '--dry-run', dest='dry_run', action='store_true', default=False,
        help='Collect the data but don"t send it to AppOptics.')
    parser.add_argument(
        '--verbose', dest='verbose', action='store_true', default=False,
        help='Output debug information.')
    parser.add_argument(
        '--token-file', dest='token_file', default='appoptics_key',
        help='Read API token from the specified file.')

    return parser.parse_args()


def get_datasources():
    """Create datasource objects."""
    return HostDatasource(), ActualWeatherDatasource(_options.location)


def run():
    global _options
    _options = _read_args()
    
    log_formatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=log_formatter)
    logging_level = logging.DEBUG if _options.verbose else logging.INFO
    logger.setLevel(logging_level)

    datasources = get_datasources()
    read_and_send(datasources)


if __name__ == '__main__':
    run()
