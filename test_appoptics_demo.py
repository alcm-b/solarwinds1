from collections import namedtuple
from unittest.mock import Mock, patch

import appoptics_demo


class MockDs(appoptics_demo.Datasource):
    """Dummy data source that serves hard-coded data tuples."""
    _data = [
        ('dkey1', 'dvalue1'),
        # ('dkey2', 'dvalue2'),
        ]
    def read(self):
        for d in self._data:
            yield d


MockAppOptions = namedtuple('toptions', 'dry_run'.split())


def test_send_data():
    """Verify that _send_data uses AppOptics API"""
    tapi, ds, opts =  Mock(), MockDs(), MockAppOptions(dry_run=False)
    with patch('appoptics_demo._options', opts) as topts:
        appoptics_demo._send_data(tapi, ds)
        tapi.submit.assert_called_once_with(*ds._data[0], tags=ds.data_tags)


def test_send_data_dry_run():
    """Verify that _send_data does not use AppOptics API in dry run mode."""
    tapi, ds, opts =  Mock(), MockDs(), MockAppOptions(dry_run=True)
    with patch('appoptics_demo._options', opts) as topts:
        appoptics_demo._send_data(tapi, ds)
        tapi.submit.assert_not_called()


@patch("appoptics_demo._options", MockAppOptions(dry_run=False))
def test_send_connects():
    """Verify that send() connects to the AppOptics server."""
    ds = MockDs(),
    with patch("appoptics_demo._connect") as tconnect:
        appoptics_demo.read_and_send(ds)
        tconnect.assert_called()


@patch("appoptics_demo._options", MockAppOptions(dry_run=False))
def test_send_uses_all_ds():
    """Verify that send() queries both supplied data sources."""
    ds_list = MockDs(), MockDs()
    for ds in ds_list:
        ds.read = Mock(side_effect=[(('key', 1),)])

    with patch("appoptics_demo._connect") as tconnect:
        appoptics_demo.read_and_send(ds_list)

    ds_list[0].read.assert_called()
    ds_list[1].read.assert_called()


def test_the_rest_of_the_application():
    assert "and so on and so forth"
