Solarwinds AppOptics demo
=========================

This is a demo application that uses AppOptics client API to load demo data
into AppOptics.

### Usage

    $ appoptics_demo --help
    usage: appoptics_demo [-h] [--location LOCATION] [--dry-run] [--verbose]
                          [--token-file TOKEN_FILE]

    AppOptics demo application.

    optional arguments:
      -h, --help            show this help message and exit
      --location LOCATION   Set ICAO airport code of the host"s physical location.
      --dry-run             Collect the data but don"t send it to AppOptics.
      --verbose             Output debug information.
      --token-file TOKEN_FILE
                            Read API token from the specified file.


### Data sources

For some strange reason, this application uses two completely unrelated data
sources:

- Linux system information (hardware sensors data from `lm_sensors`, system
  load average).
- Actual weather data for the airport of choice, by default it's LKTB (Brno
  Turany).

New data sources can be added by implementing subclasses of the class
`DataSource` and using them in the function `appoptics_demo.get_datasources()`.


Installation
------------

### Requirements

- A Linux system with the folling software installed:
    - lm_sensors 3.4.0-7+
    - Python 3.5+
- A valid AppOptics API key.
- Python modules, see `requirements.txt`.


### Installation process

1. Install the application:

    `pip3 install git+https://alcm-b@bitbucket.org/alcm-b/solarwinds1.git`

2. Provide an API token from https://my.appoptics.com/organization/tokens:

    `cat <APIKEY> > /home/username/appoptics_key`

3. Run the application in dry run mode, when the data is collected but it isn't
   actually sent to AppOptics:

    `appoptics_demo --dry-run`

4. Schedule application execution with the tool of your choice (cron, systemd)
   Here is an example or a cron job:

    `* * * * * ~/.local/bin/appoptics_demo --token-file /home/username/appoptics_key >> /tmp/appoptics_demo.log 2>&1`

5. Configure charts in AppOptics UI. Here is an example of a dashboard the uses
   data from `appoptics_demo`:

   ![screenshot](doc/demo_dashboard.png)


### Limitations

Charts are to be configured manually. The default API token apparently does not
allow using Space part of the API:

    In [2]: import appoptics_metrics
    In [3]: api = appoptics_metrics.connect('b9aaatokentoken0af4b48tokentokenf4408etokentokence2tokentokenb01')
    In [4]: space = api.create_space("space1")
    ---------------------------------------------------------------------------
    [...]
    Forbidden: [403] request: This token is not permitted to perform this action on this resource

To use the `*_space()` API methods, you can create a token that has 'Full
access' (something which I did not have a chance to try).


Development
-----------

### Documentation

- [AppOptics API reference](https://docs.appoptics.com/api/)
- this file, `README.md`

### Logging

The default level of logging is 'info'. Use CLI option `--verbose` to increase
the logging level to `DEBUG`.

### Error reporting

- Connection errors to AppOptics are fatal errors. setting `--verbose` option
  enable printing the stack traces.
- TODO properly report data collection errors

### Tests

How to run unit tests:

    pip3 install -r test_requirements.txt
    pytest ./test_appoptics_demo.py

There is currently just a single test case that does nothing. Unit tests are
in TODO.

### Packaging and distribution

Applicaion comes with setuptools script `setup.py` which can be used for
installing the application directly from git repo or packaging it with
setuptools.

Distribution is not in the plans: it's a silly demo app, after all. It is
technically possible to submit this package to pypi, though.


### Development environment

This is how to configure development environment:

    mkvirtualenv appoptics
    git clone https://alcm-b@bitbucket.org/alcm-b/solarwinds1.git
    cd solarwinds1
    pip3 install -r requirements.txt
    pip3 install -r test_requirements.txt

Installation of the Python module `pyjq` may require these packages to be installed
first:

    dnf install -y autoconf automake libtool


### TODO

- Enhance logging of data source errors.
- Provide a sample configuration of a systemd service that could run this app
  on schedule.
- Refactor query string that is hard coded in `requests.get()` parameters;
  handle possible HTTP errors.
- Add more unit tests.
