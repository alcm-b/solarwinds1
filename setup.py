import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='appoptics_demo',
    version='0.1',
    url='https://bitbucket.org/alcm-b/solarwinds1',
    author='Egor Berdnikov',
    author_email='egor.berdnikov@gmail.com',
    license='MIT',
    description='A demo application that uses AppOptics client API',
    long_description=long_description,
    long_description_content_type="text/markdown",
    platforms=['POSIX'],
    entry_points=dict(
        console_scripts=['appoptics_demo = appoptics_demo:run']
    ),
    install_requires=[
        "appoptics_metrics",
        "pyjq",
        "requests"
    ],
    tests_require=["pytest"],
    py_modules=['appoptics_demo'],
    classifiers=(
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Topic :: Utilities",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ),
)
